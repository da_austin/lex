from sqlalchemy import *
import logging
from db import engine,session

logger = logging.getLogger(__name__)

def execute_proc(proc_name, params):
    ''' 
    helper to run postgres functions with provided parameters
    '''
    param_str = ','.join(str(x) for x in params)
    statement = "select * from %s(%s)" % (proc_name, param_str)
    logger.debug(statement)
    records = session.execute(statement)
    logger.debug('Session complete.')
    return records

def stores_most_abuse_spend(comp_id, state, period):
    '''
    run function most_abuse_spend
    get stores with most spending on abuse calls
    comp_id -- Id of customer
    period -- duration (months)
    '''
    params = []
    params.append(comp_id)
    params.append(state)
    params.append(period)
    proc = 'most_abuse_spend'

    results = execute_proc(proc, params)
    logger.debug('Query complete.')
    
    if results.rowcount == 0:
        return ''
    return rs_to_str(results)
                    

def rs_to_str(rs):
    # 
    keys = rs.keys()
    out_str = '\n'
    for idx, row in enumerate(rs):
        out_str += str(idx + 1)
        for i in range(len(keys)):
            out_str += ' || ' + keys[i] + ': ' + str(row[i])
        out_str += '\n'  
        
    return out_str






