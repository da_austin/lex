import dateutil.parser
import datetime
import time
import os
import logging
import db_ops

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


""" --- Helpers to build responses which match the structure of the necessary dialog actions --- """

comp_dict = {
    'lowes' : 1,
    'homedepot' : 2,
    'walmart' : 3
}

state_dict = {
    'alabama': 'al',
    'alaska': 'ak',
    'arizona': 'az',
    'arkansas': 'ar',
    'california': 'ca',
    'colorado': 'co',
    'connecticut': 'ct',
    'delaware': 'de',
    'florida': 'fl',
    'georgia': 'ga',
    'hawaii': 'hi',
    'idaho': 'id',
    'illinois': 'il',
    'indiana': 'in',
    'iowa': 'ia',
    'kansas': 'ks',
    'kentucky': 'ky',
    'louisiana': 'la',
    'maine': 'me',
    'maryland': 'md',
    'massachusetts': 'ma',
    'michigan': 'mi',
    'minnesota': 'mn',
    'mississippi': 'ms',
    'missouri': 'mo',
    'montana': 'mt',
    'nebraska': 'ne',
    'nevada': 'nv',
    'new hampshire': 'nh',
    'new jersey': 'nj',
    'new mexico': 'nm',
    'new york': 'ny',
    'north carolina': 'nc',
    'north dakota': 'nd',
    'ohio': 'oh',
    'oklahoma': 'ok',
    'oregon': 'or',
    'pennsylvania': 'pa',
    'rhode island': 'ri',
    'south carolina': 'sc',
    'south dakota': 'sd',
    'tennessee': 'tn',
    'texas': 'tx',
    'utah': 'ut',
    'vermont': 'vt',
    'virginia': 'va',
    'washington': 'wa',
    'west virginia': 'wv',
    'wisconsin': 'wi',
    'wyoming': 'wy'
}

def get_slots(intent_request):
    return intent_request['currentIntent']['slots']


def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit, message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message
        }
    }


def close(session_attributes, fulfillment_state, message):
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    }

    return response


def delegate(session_attributes, slots):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Delegate',
            'slots': slots
        }
    }


""" --- Helper Functions --- """

def build_validation_result(is_valid, violated_slot, message_content):
    if message_content is None:
        return {
            "isValid": is_valid,
            "violatedSlot": violated_slot,
        }

    return {
        'isValid': is_valid,
        'violatedSlot': violated_slot,
        'message': {'contentType': 'PlainText', 'content': message_content}
    }


def isvalid_state(state):
    if len(state) == 2:
        if state in state_dict.values():
            return True
    else:
        if state in state_dict.keys():
            return True

    return False

def get_state_cd(state):
    st = state.lower()
    if st not in state_dict:
        return st
    else:
        return state_dict[st]

def validate_find_stores(customer, months, state):
    customers = ['lowes', 'homedepot', 'walmart']
    if customer is not None:
        cust_str = ''.join(filter(str.isalpha, customer))
        if cust_str.lower() not in customers:
            return build_validation_result(False,
                                           'Customer',
                                           'We do not have {}, would you like a different customer?  '
                                           'Our main customers include Lowes, Walmart, and Home Depot'.format(customer))

    if state is not None:
        if not isvalid_state(state.lower()):
            return build_validation_result(False, 'State', 'Not a valid State, please enter a correct US state?')

    return build_validation_result(True, None, None)


""" --- Functions that control the bot's behavior --- """


def most_abuse(intent_request):
    """
    Performs dialog management and fulfillment for finding stores.
    Beyond fulfillment, the implementation of this intent demonstrates the use of the elicitSlot dialog action
    in slot validation and re-prompting.
    """

    customer = get_slots(intent_request)["Customer"]
    months = get_slots(intent_request)["Period"]
    state = get_slots(intent_request)["State"]

    source = intent_request['invocationSource']

    if source == 'DialogCodeHook':
        # Perform basic validation on the supplied input slots.
        # Use the elicitSlot dialog action to re-prompt for the first violation detected.
        slots = get_slots(intent_request)

        validation_result = validate_find_stores(customer, months, state)
        if not validation_result['isValid']:
            slots[validation_result['violatedSlot']] = None
            return elicit_slot(intent_request['sessionAttributes'],
                               intent_request['currentIntent']['name'],
                               slots,
                               validation_result['violatedSlot'],
                               validation_result['message'])

        # Pass the price of the flowers back through session attributes to be used in various prompts defined
        # on the bot model.
        output_session_attributes = intent_request['sessionAttributes'] if intent_request['sessionAttributes'] is not None else {}

        return delegate(output_session_attributes, get_slots(intent_request))

    # Run query to get stores
    # Results for output
    cust = ''.join(filter(str.isalpha, customer)).lower()
    comp_id = comp_dict.get(cust)
    output_result = db_ops.stores_most_abuse_spend(comp_id, "'" + get_state_cd(state) + "'", months)
    logger.debug(output_result)
    if output_result == '':
        output_content = 'No store found in {} within {} months for {} with abuse calls'.format(state, months, customer)
    else:
        output_content = 'Here are the top {} stores from {} in the last {} months.{}'.format(customer, state, months, output_result)
    logger.debug(output_content)
    return close(intent_request['sessionAttributes'],
                 'Fulfilled',
                 {'contentType': 'PlainText',
                  'content': output_content})


""" --- Intents --- """

def dispatch(intent_request):
    """
    Called when the user specifies an intent for this bot.
    """

    logger.debug('dispatch userId={}, intentName={}'.format(intent_request['userId'], intent_request['currentIntent']['name']))

    intent_name = intent_request['currentIntent']['name']

    # Dispatch to your bot's intent handlers
    if intent_name == 'GetMostAbuseSpending':
        return most_abuse(intent_request)

    raise Exception('Intent with name ' + intent_name + ' not supported')


""" --- Main handler --- """

def lambda_handler(event, context):

    os.environ['TZ'] = 'America/Chicago'
    time.tzset()
    logger.debug('event.bot.name={}'.format(event['bot']['name']))

    return dispatch(event)
