import unittest

import db_ops
import handler
from handler import comp_dict


@unittest.skip("classing skipping")
class TestCompDict(unittest.TestCase):
    def test(self):
        co = 'Wal-Mart'
        cust = ''.join(filter(str.isalpha, co)).lower()
        comp_id = comp_dict.get(cust)
        rs = db_ops.stores_most_abuse_spend(comp_id, 3)
        print(rs)

@unittest.skip("classing skipping")
class TestStateValidator(unittest.TestCase):
    def test(self):
        state = 'Texas'
        print(handler.isvalid_state(state.lower()))

@unittest.skip("classing skipping")
class TestState(unittest.TestCase):
    def test(self):
        st = "Texas"
        print(handler.get_state_cd(st))

@unittest.skip("classing skipping")
class TestRunAbuse(unittest.TestCase):

    def test(self):
        state = 'Texas'
        st = handler.get_state_cd(state)
        print(st)
        records = db_ops.stores_most_abuse_spend(1, "'" + st + "'", 24)
        print(records)

if __name__ == '__main__':
    unittest.main()


